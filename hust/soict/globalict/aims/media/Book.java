package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public Book() {

    }

    public Book(int id) {
        super(id);
    }

    public Book(int id, String title) {
        super(id, title);
    }

    public Book(int id, String title, String category) {
        super(id, title, category);
    }

    public Book(int id, String title, String category, List<String> authors) {
        super(id, title, category);
        this.authors = authors;
    }

    public void addAuthor(String authorName) {
        boolean check = true;
        for (String i : authors) {
            if (i.equals(authorName))
                check = false;
        }

        if (check) {
            authors.add(authorName);
            System.out.println("Add author successfully!");
        } else {
            System.out.println("Author is already existed!");
        }
    }

    public void removeAuthor(String authorName) {
        int check = -1;
        for (int i = 0; i < authors.size(); ++i) {
            if (authors.get(i).equals(authorName))
                check = i;
        }

        if (check == -1) {
            System.out.println("Author is not existed!");
        } else {
            authors.remove(check);
            System.out.println("Remove author successfully!");
        }
    }
}
