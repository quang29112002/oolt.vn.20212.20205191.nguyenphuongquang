package hust.soict.globalict.aims.media;

public class Track extends Disc implements Playable {
    public Track() {
    }

    public Track(int id) {
        super(id);
    }

    public Track(int id, String title) {
        super(id, title);
    }

    public Track(int id, String title, String category) {
        super(id, title, category);
    }

    public Track(int id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public Track(int id, String title, String category, float cost, float length) {
        super(id, title, category, cost, length);
    }

    public Track(int id, String title, String category, float cost, float length, String director) {
        super(id, title, category, cost, length, director);
    }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

}
