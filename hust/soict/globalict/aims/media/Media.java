package hust.soict.globalict.aims.media;

public class Media implements Comparable<Object> {
    public int id;
    private String title;
    private String category;
    private float cost;

    public Media() {

    }

    public Media(int id) {
        this.id = id;
    }

    public Media(int id, String title) {
        this(id);
        this.title = title;
    }

    public Media(int id, String title, String category) {
        this(id, title);
        this.category = category;
    }

    public Media(int id, String title, String category, float cost) {
        this(id, title, category);
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public int compareTo(Object objCompare) {
        Media other = (Media)objCompare;
        return this.id = other.getId();
    }
}
