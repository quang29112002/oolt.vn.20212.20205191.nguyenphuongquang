package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {

    public DigitalVideoDisc() {

    }

    public DigitalVideoDisc(int id) {
        super(id);
    }

    public DigitalVideoDisc(int id, String title) {
        super(id, title);
    }

    public DigitalVideoDisc(int id, String title, String category) {
        super(id, title, category);
    }

    public DigitalVideoDisc(int id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public DigitalVideoDisc(int id, String title, String category, float cost, float length) {
        super(id, title, category, cost, length);
    }

    public DigitalVideoDisc(int id, String title, String category, float cost, float length, String director) {
        super(id, title, category, cost, length, director);
    }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public boolean search(String searchTitle) {
        String[] stringArray = this.getTitle().split(" ");
        String[] searchTitleArray = searchTitle.split(" ");
        boolean[] check = new boolean[searchTitleArray.length];
        for (int i = 0; i < searchTitleArray.length; ++i) {
            check[i] = false;
            for (String t : stringArray) {
                if (t.toLowerCase().contains(searchTitleArray[i].toLowerCase())) {
                    check[i] = true;
                    break;
                }
            }
        }

        boolean finalCheck = true;
        for (int i = 0; i < searchTitleArray.length; ++i) {
            if (check[i] == false) {
                finalCheck = false;
            }
        }

        return finalCheck;
    }
}
