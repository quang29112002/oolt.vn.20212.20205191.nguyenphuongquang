package hust.soict.globalict.aims.media;

public class Disc extends Media {
    private float length;
    private String director;

    public Disc() {

    }

    public Disc(int id) {
        super(id);
    }

    public Disc(int id, String title) {
        super(id, title);
    }

    public Disc(int id, String title, String category) {
        super(id, title, category);
    }

    public Disc(int id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public Disc(int id, String title, String category, float cost, float length) {
        super(id, title, category, cost);
        this.length = length;
    }

    public Disc(int id, String title, String category, float cost, float length, String director) {
        this(id, title, category, cost, length);
        this.director = director;
    }

    public float getLength() {
        return this.length;
    }

    public String getDirector() {
        return this.director;
    }
}
