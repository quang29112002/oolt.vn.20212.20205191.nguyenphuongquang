package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public CompactDisc(int id) {
        super(id);
    }

    public CompactDisc(int id, String title) {
        super(id, title);
    }

    public CompactDisc(int id, String title, String category) {
        super(id, title, category);
    }

    public String getArtist() {
        return this.artist;
    }

    public void play() {
        for (Track i : tracks) {
            i.play();
        }
    }

    public void addTrack(Track track) {
        boolean found = false;
        for (Track i : tracks) {
            if (i.equals(track)) {
                found = true;
            }
        }

        if (found) {
            System.out.println("Track is already existed!");
        } else {
            tracks.add(track);
        }
    }

    public void removeTrack(Track track) {
        int found = -1;
        for (int i = 0; i < tracks.size(); ++i) {
            if (tracks.get(i).equals(track)) {
                found = i;
            }
        }

        if (found == -1) {
            System.out.println("Track is not existed!");
        } else {
            tracks.remove(found);
            System.out.println("Romove track successfully!");
        }
    }

    public float getLength() {
        float sumOfLength = 0;
        for (Track i : tracks) {
            sumOfLength += i.getLength();
        }
        return sumOfLength;
    }
}
