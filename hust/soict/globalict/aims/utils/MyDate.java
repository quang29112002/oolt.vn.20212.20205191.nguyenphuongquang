package hust.soict.globalict.aims.utils;

import java.time.LocalDate;
import java.util.Scanner;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        LocalDate date = LocalDate.now();
        String[] data = date.toString().split("-");
        this.day = Integer.parseInt(data[2]);
        this.month = Integer.parseInt(data[1]);
        this.year = Integer.parseInt(data[0]);
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void accept() {
        System.out.print("Enter the date in the format dd/mm/yyyy: ");
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();
        String[] dateSplit = date.split("/");
        this.day = Integer.parseInt(dateSplit[0]);
        this.month = Integer.parseInt(dateSplit[1]);
        this.year = Integer.parseInt(dateSplit[2]);
        scanner.close();
    }

    public void print() {
        System.out.println(this.day + "/" + this.month + "/" + this.year);
    }

}
