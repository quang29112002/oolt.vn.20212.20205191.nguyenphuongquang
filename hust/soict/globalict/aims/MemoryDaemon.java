package hust.soict.globalict.aims;
import java.lang.Runtime;

public class MemoryDaemon {
    private float memoryUsed = 0;

    public void run() {
        Runtime rt = Runtime.getRuntime();
        long used;

        while(true) {
            used = rt.totalMemory() - rt.freeMemory();
            if(used != memoryUsed) {
                System.out.println("\tMemory used  = " + used);
                this.memoryUsed = used;
            }
        }
    }
}
