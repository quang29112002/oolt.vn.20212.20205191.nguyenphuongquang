package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;

public class Aims {
    public static int numberOfOrder = 0;

    public static void showMenu() {

        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");

    }

    public static void main(String[] args) throws Exception {
        ArrayList<Order> theOrder = new ArrayList<Order>();
        int choice;

        while (true) {
            showMenu();
            Scanner scanner = new Scanner(System.in);

            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.println("Enter id for order: ");
                    int newId = scanner.nextInt();
                    Order newOrder = new Order(newId);
                    theOrder.add(newOrder);
                    break;

                case 2: {
                    System.out.println("Enter order id: ");
                    int searchId = scanner.nextInt();

                    for (Order i : theOrder) {
                        if (searchId == i.getId()) {
                            System.out.println("Found!");

                            System.out.print(
                                    "Enter the type of item you want to add(Book, CompactDisc or DigitalVideoDisc): ");
                            scanner = new Scanner(System.in);
                            String type = scanner.nextLine();

                            System.out.println("Enter information: ");
                            scanner.close();
                            System.out.print("Id: ");
                            scanner = new Scanner(System.in);
                            int id = scanner.nextInt();

                            scanner.close();
                            System.out.print("Title: ");
                            scanner = new Scanner(System.in);
                            String title = scanner.nextLine();

                            scanner.close();
                            System.out.print("Category: ");
                            scanner = new Scanner(System.in);
                            String category = scanner.nextLine();

                            if (type == "Book") {
                                Book newBook = new Book(id, title, category);
                                i.addMedia(newBook);
                            } else if (type == "DigitalVideoDisc") {
                                scanner.close();
                                System.out.print("Cost: ");
                                scanner = new Scanner(System.in);
                                float cost = scanner.nextFloat();

                                scanner.close();
                                System.out.print("Length: ");
                                scanner = new Scanner(System.in);
                                float length = scanner.nextFloat();

                                scanner.close();
                                System.out.print("Director: ");
                                scanner = new Scanner(System.in);
                                String director = scanner.nextLine();

                                DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, cost, length,
                                        director);
                                i.addMedia(dvd);
                            } else if (type == "CompactDisc") {
                                CompactDisc newMedia = new CompactDisc(id, title, category);
                                scanner.close();
                                System.out.println("Enter the number of tracks: ");
                                scanner = new Scanner(System.in);
                                int numTrack = scanner.nextInt();

                                for (int j = 0; j < numTrack; j++) {
                                    System.out.println("Enter information: ");
                                    scanner.close();
                                    System.out.print("Id: ");
                                    scanner = new Scanner(System.in);
                                    int idTrack = scanner.nextInt();

                                    scanner.close();
                                    System.out.print("Title: ");
                                    scanner = new Scanner(System.in);
                                    String titleTrack = scanner.nextLine();

                                    scanner.close();
                                    System.out.print("Category: ");
                                    scanner = new Scanner(System.in);
                                    String categoryTrack = scanner.nextLine();

                                    Track newTrack = new Track(idTrack, titleTrack, titleTrack);
                                    newMedia.addTrack(newTrack);
                                }

                                i.addMedia(newMedia);

                            }

                            break;
                        }
                    }

                    System.out.println("Cannot find the order!");

                    break;
                }
                case 3: {
                    System.out.println("Enter order id: ");
                    scanner.close();
                    scanner = new Scanner(System.in);
                    int searchId = scanner.nextInt();
                    for (Order i : theOrder) {
                        if (i.getId() == searchId) {
                            System.out.println("Found!");
                            System.out.println("Enter information: ");
                            System.out.print("Id: ");
                            int id = scanner.nextInt();
                            i.removeMedia(id);
                            break;
                        }
                    }
                    System.out.println("Cannot find the order!");
                    break;
                }
                case 4: {
                    scanner.close();
                    System.out.println("Enter order id: ");
                    scanner = new Scanner(System.in);
                    int searchId = scanner.nextInt();
                    for (Order i : theOrder) {
                        if (i.getId() == searchId) {
                            System.out.println("Found!");
                            i.displayItem();
                        }
                    }
                    System.out.println("Cannot find the order!");
                    break;
                }
                case 0:
                    scanner.close();
                    return;
            }

            scanner.close();
        }
    }
}
