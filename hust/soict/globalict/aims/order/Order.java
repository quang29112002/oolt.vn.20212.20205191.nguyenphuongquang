package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.Media;

import java.time.LocalDate;
import java.util.ArrayList;

public class Order {
    private int id;
    private static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERED = 5;

    public LocalDate dateOrdered;

    public static int nbOrders = 0;

    ArrayList<Media> listDisc = new ArrayList<Media>();

    public Order(int id) {
        this.dateOrdered = LocalDate.now();
        this.id = id;
    }

    public int qtyOrdered() {
        return listDisc.size();
    }

    public int getId() {
        return this.id;
    }

    public void displayItem() {
        System.out.println("******");
        for (Media i : listDisc) {
            System.out.println("Id: " + i.getId());
            System.out.println("Title: " + i.getTitle());
            System.out.println("Category: " + i.getCategory());
            System.out.println("Cost: " + i.getCost());
            System.out.println("******");
        }
    }

    public void addMedia(Media disc) {
        boolean notMax = true;
        if (listDisc.size() == MAX_NUMBERS_ORDERED) {
            System.out.println("The order is almost full");
        } else {
            for (Media item : listDisc) {
                if (item.equals(disc)) {
                    notMax = false;
                    break;
                }
            }

            if (notMax) {
                listDisc.add(disc);
            }
        }

    }

    public void addMedia(Media[] dvdList) {
        boolean existed = false;
        if (listDisc.size() == MAX_NUMBERS_ORDERED) {
            System.out.println("The order is almost full");
        } else if (listDisc.size() + dvdList.length > MAX_NUMBERS_ORDERED) {
            System.out.println("The item quantity has reached its limit!");
        } else {
            for (Media item : dvdList) {
                for (Media i : listDisc) {
                    existed = false;
                    if (i.equals(item)) {
                        System.out.println("DVD " + item.getTitle() + " is existed!");
                        existed = true;
                    }
                }
                if (existed == false)
                    listDisc.add(item);
            }
        }
    }

    public void addMedia(Media dvd1, Media dvd2) {
        if (listDisc.size() == MAX_NUMBERS_ORDERED) {
            System.out.println("The order is almost full");
        } else if (listDisc.size() + 2 > MAX_NUMBERS_ORDERED) {
            System.out.println("The item quantity has reached its limit!");
        } else {
            boolean dvd1Existed = false;
            boolean dvd2Existed = false;
            for (Media item : listDisc) {
                if (dvd1.equals(item)) {
                    System.out.println("DVD " + dvd1.getTitle() + " is existed!");
                    dvd1Existed = true;
                }
                if (dvd2.equals(item)) {
                    System.out.println("DVD " + dvd2.getTitle() + " is existed!");
                    dvd2Existed = true;
                }
            }
            if (dvd1Existed == false) {
                listDisc.add(dvd1);
            }
            if (dvd2Existed == false) {
                listDisc.add(dvd2);
            }
        }
    }

    public void removeMedia(int id) {
        int size = listDisc.size();
        boolean notExist = true;
        for (int i = 0; i < size; ++i) {
            if (listDisc.get(i).getId() == id) {
                listDisc.remove(i);
                notExist = false;
                System.out.println("Remove successfully");
            }
        }

        if (notExist) {
            System.out.println("Disc is not existed!");
        }
    }

    public float totalCost() {
        float sum = 0;
        for (Media item : listDisc) {
            sum += item.getCost();
        }

        return sum;
    }

    public Media getALuckyItem() {
        int rand = (int) (Math.random() * qtyOrdered());
        Media freeDisc = listDisc.get(rand);
        for (Media i : listDisc) {
            if (i.equals(freeDisc)) {
                i.setCost(0);
            }
        }
        return freeDisc;
    }
}
