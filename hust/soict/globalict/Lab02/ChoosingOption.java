package hust.soict.globalict.Lab02;

import javax.swing.JOptionPane;

public class ChoosingOption {
    public static void main(String[] args) throws Exception {
        int option = JOptionPane.showConfirmDialog(null, "Do you want to change to the first class ticket?");
        JOptionPane.showMessageDialog(null, "You'vs chosen:" + (option == JOptionPane.YES_OPTION ? "Yes" : "No"));
        System.exit(0);
    }
}
