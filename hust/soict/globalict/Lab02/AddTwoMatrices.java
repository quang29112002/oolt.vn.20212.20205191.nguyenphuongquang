package hust.soict.globalict.Lab02;

import java.util.Scanner;

public class AddTwoMatrices {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows and columns: ");
        int m = scanner.nextInt(), n = scanner.nextInt();
        double[][] arr1 = new double[m][n];
        double[][] arr2 = new double[m][n];

        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j) {
                System.out.printf("Arr1[%d][%d]: ", i + 1, j + 1);
                arr1[i][j] = scanner.nextDouble();
            }

        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j) {
                System.out.printf("Arr2[%d][%d]: ", i + 1, j + 1);
                arr2[i][j] = scanner.nextDouble();
            }

        System.out.println("Arr1: ");
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {

                System.out.print(arr1[i][j] + " ");
            }
            System.out.print("\n");
        }

        System.out.println("Arr2: ");
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {

                System.out.print(arr2[i][j] + " ");
            }
            System.out.print("\n");
        }

        System.out.println("Sum: ");
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {

                System.out.print((arr1[i][j] + arr2[i][j]) + " ");
            }
            System.out.print("\n");
        }

        scanner.close();
    }
}
