package hust.soict.globalict.Lab02;

import java.util.Arrays;
import java.util.Scanner;

public class SortArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of elements: ");
        int m = scanner.nextInt();

        double[] arr1 = new double[m];

        System.out.println("Enter the value (seperated by a space): ");

        for (int i = 0; i < m; ++i) {
            arr1[i] = scanner.nextDouble();
        }

        Arrays.sort(arr1); 

        System.out.println("Sorted array: " +  Arrays.toString(arr1) );

        {
            double sum = 0;
            for (int i = 0; i < m; ++i) {
                sum += arr1[i];
            }
            System.out.println("Sum = " + sum);
            System.out.println("Average value = " + sum/m);
        }

        scanner.close();
    }
}
