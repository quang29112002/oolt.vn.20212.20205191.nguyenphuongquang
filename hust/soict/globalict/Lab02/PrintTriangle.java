package hust.soict.globalict.Lab02;

import java.util.Scanner;

public class PrintTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the height of triangle: ");
        int height = scanner.nextInt();

        for(int i = 0; i < height; ++i) {
            for(int j = 0; j< height-i; ++j) {
                System.out.print(" ");
            }

            for(int j = 0; j < 2*i+1; ++j) {
                System.out.print("*");
            }

            for(int j = 0; j< height-i; ++j) {
                System.out.print(" ");
            }
            System.out.printf("\n");
        }

        scanner.close();
    }
}
