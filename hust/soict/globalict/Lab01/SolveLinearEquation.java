package hust.soict.globalict.Lab01;

import java.util.Scanner;
public class SolveLinearEquation {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a and b for equation ax+b=0");

        System.out.println("Enter a: ");
        double num1 = scanner.nextDouble();

        System.out.println("Enter b: ");
        double num2 = scanner.nextDouble();

        if(num1 == 0 && num2 == 0) {
            System.out.println("The equation has many results");
            System.exit(0);
        }

        if(num1 == 0 && num2 != 0) {
            System.out.println("The equation has no result");
            System.exit(0);
        }

        if(num1 != 0 && num2 != 0) {
            System.out.println("The result is " + -num2/num1);
            System.exit(0);
        }

        scanner.close();
    }
}
