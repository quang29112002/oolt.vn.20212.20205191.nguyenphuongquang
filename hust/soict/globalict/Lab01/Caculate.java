package hust.soict.globalict.Lab01;

import javax.swing.JOptionPane;

public class Caculate {
    public static void main(String[] args) {

        double num1 = Double.parseDouble(JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
        JOptionPane.INFORMATION_MESSAGE));

        double num2 = Double.parseDouble(JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
        JOptionPane.INFORMATION_MESSAGE));

        JOptionPane.showMessageDialog(null, "Sum = " + (num1 + num2));
        JOptionPane.showMessageDialog(null, "Difference = " + Math.abs(num1 - num2));
        JOptionPane.showMessageDialog(null, "Product = " + (num1 * num2));
        JOptionPane.showMessageDialog(null, "Quotient = " + (num1 / num2));

        System.exit(0);
    }
}
