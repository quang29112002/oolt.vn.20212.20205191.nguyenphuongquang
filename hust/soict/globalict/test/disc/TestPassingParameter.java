package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
    public static void swap(Object o1, Object o2) {
        Object tmp = o1;
        o1 = o2;
        o2 = tmp;
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        int id = dvd.getId();
        // dvd.setTitle(title);
        dvd = new DigitalVideoDisc(id, oldTitle);
    }
}
